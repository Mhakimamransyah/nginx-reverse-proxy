const configEnv = require('dotenv')
const express   = require('express')
const app       = express()

configEnv.config({path:'../.env'})


app.get('/', (req, res) => {
    res.send({
        "Messages": "Hello from app 3",
    });
})

app.listen(parseInt(process.env.NODE_PORT),'0.0.0.0', () => {
    console.log("Express JS, listening on "+process.env.NODE_PORT);
})

