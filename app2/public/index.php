<?php

require __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable("../");
$dotenv->load();

$app = new FrameworkX\App();

$app->get('/', function () {
    return React\Http\Message\Response::json([
        "Messages" => "Hello from app 2"
    ]);
});


$app->run();