# using latest nginx image
FROM nginx:latest

# copy nginx main config
COPY ./nginx.conf /etc/nginx/nginx.conf

# Delete defualt conf
RUN rm /etc/nginx/conf.d/default.conf

# COPY virtual server config to conf.d nginx
COPY ./config/*.conf /etc/nginx/conf.d/
