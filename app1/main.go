package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
)

func main() {

	if err := godotenv.Load("../.env"); err != nil {
		panic(err)
	}

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.JSON(map[string]string{
			"Messages": "Hello from app 1",
		})
	})

	port, err := strconv.Atoi(os.Getenv("GO_PORT"))

	if err != nil {
		panic(err)
	}

	app.Listen(fmt.Sprintf(":%d", port))
}
